import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { HijoComponent } from '../hijo/hijo.component'

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements AfterViewInit {

  mensajeP!: string;
  @ViewChild (HijoComponent) hijo: any; //se le pone cualquier nombre, aqui le pusimos hijo: este es un objeto

  mensaje2!: string;
  @ViewChild (HijoComponent) hijo2: any;

  mensajeJ!: any;
  @ViewChild (HijoComponent) hijoJ : any;

  mensajeJ2!: any;
  @ViewChild (HijoComponent) hijoJ2 : any;

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }
  
  ngAfterViewInit(){
    this.mensajeP = this.hijo.mensajeH;
    console.log(this.mensajeP);
    this.cdr.detectChanges();
    

    this.mensaje2 = this.hijo2.mensaje2;
    this.cdr.detectChanges();

    this.mensajeJ = this.hijoJ.json1;
    this.cdr.detectChanges();

    this.mensajeJ2 = this.hijoJ2.json2;
    this.cdr.detectChanges()
  }
  
}
