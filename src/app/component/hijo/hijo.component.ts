import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  mensajeH: string = 'Mensaje desde el hijo'
  mensaje2: string = 'Mensaje 2 del hijo'
  json1: any = {
    nombre: 'alba',
    CI: 1253478
  }
  json2: any = {
    titulo: 'La noche oscura',
    escritor: 'Jorge Luis',
    fecha: '23-jun-2013'
  }

  constructor() { }

  ngOnInit(): void {
  }

}
